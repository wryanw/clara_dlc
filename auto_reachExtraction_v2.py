#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 14:20:20 2019

@author: spencer
"""
from __future__ import print_function
import os
from pathlib import PurePath
import glob
import numpy as np
from pathlib import Path
import pandas as pd
from deeplabcut.utils import auxiliaryfunctions
from deeplabcut.CLARA_DLC import CLARA_DLC_utils as clara
from datetime import timedelta, date

def prep():
    prev_date_list = list()
    start_dt = date(2019, 9, 11)
    end_dt = date.today()

    for n in range(int ((end_dt - start_dt).days)+1):
        dt = start_dt+ timedelta(n)
        prev_date_list.append(dt.strftime("%Y%m%d"))
        
    user_cfg = clara.read_config()
    server_dir = '/run/user/10*'
    mounted_serv = glob.glob(server_dir)
    if not len(mounted_serv):
        print('No server found')
    syntag = '/gvfs/smb-share:server=10.33.107.41,share=whsynology/BIOElectricsLab/RAW_DATA/AutomatedBehavior'
    synology = PurePath(mounted_serv[0] + syntag)
    if not os.path.isdir(synology):
        print('Synology not found')
    
    dirlist = list()
    for f in prev_date_list:
        unit_dirR = os.path.join(synology, f, user_cfg['unitRef'])
        if os.path.exists(unit_dirR):
            prev_expt_list = [name for name in os.listdir(unit_dirR)]
            for s in prev_expt_list:
                dirlist.append(os.path.join(unit_dirR, s))

    for s in dirlist:
        vid_list = os.path.join(s, '*.mp4')
        videos = glob.glob(vid_list)
        if len(videos) == 3:
            extract(videos)

def extract(videos):
    
# =============================================================================
    overwrite = True
# =============================================================================
    
    
    datestr = os.path.split(videos[0])[1].split('_unit')[0]
    rch_sel = 0
    act_sel = 3
    if datestr >= '20190924':
        findP = False
    else:
        findP = True
        
    user_cfg = clara.read_config()
    config_path = user_cfg['config_path']
    efile = list()
    ffile = list()
    pellet_state = 0
    sys_timer = [0,0,0]
    videoList = videos
    vidDir = os.path.split(videoList[0])[0]
    
    cfg = auxiliaryfunctions.read_config(config_path)
    currFrame = 0
    bodyparts = cfg['bodyparts']
    # checks for unique bodyparts
    if len(bodyparts)!=len(set(bodyparts)):
        print("Error - bodyparts must have unique labels! Please choose unique bodyparts in config.yaml file and try again.")
    
    videos = list()
    videoOrder = ['side','front','top']
    for key in enumerate(videoOrder):
        for video in enumerate(videoList):
            if key[1] in video[1]:
                videos.append(video[1])
                
    cropPts = list()
    numberFrmList = list()
    
    if isinstance(bodyparts,list):
        parts = bodyparts
    else:
        parts = list()
        categories = list()
        for cat in bodyparts.keys():
            categories.append(cat)
        for key in categories:
            for ptname in bodyparts[key]:
                parts.append(ptname)
    bodyparts = parts
    
    
    for vndx, video in enumerate(videos):
        cpt = user_cfg[videoOrder[vndx]+'Crop']
        cropPts.append(cpt)
        
    efile = os.path.join(vidDir, '*events.txt')
    efile = glob.glob(efile)
    if len(efile) and os.path.isfile(efile[0]):
        eventList = list()
        eventTab = 0
        with open(efile[0]) as fp:
            for line in fp:
                eventList.append(line)
    else:
        findP = True
    
    ffile = videos[2].split('topCam')[0] + 'reachAnno.txt'
    ffile = ffile.split('RAW_DATA')[0]+'ANALYZED_DATA/AutomatedBehavior/ReachingEvents/'+os.path.split(ffile)[1]
    if len(ffile) and os.path.isfile(ffile[0]):
        if not overwrite:
            return
    print(os.path.split(ffile)[1])
    
    videoSrc = videos[0]
    vidDir, vidName = os.path.split(videoSrc)
    vidName, vidExt = os.path.splitext(vidName)
    onlyfiles = [f for f in os.listdir(vidDir) if os.path.isfile(os.path.join(vidDir, f))]
    h5files = [h for h in onlyfiles if '.h5' in h]
    h5parts = [(m.split('DeepCut')[1]) for m in h5files]
    if not len(h5parts):
        print('No annotations found')
        return
    h5tag = '_resnet50_RTDLC_SimClustSep11shuffle1_1030000.h5'
    scorer = 'DeepCut%s' % os.path.splitext(h5tag)[0]
    h5list = ['%s%s.h5' % (j, scorer) for j in [os.path.splitext(v)[0] for v in videos]]
    df_likelihood = list()
    df_x = list()
    df_y = list()
    
    PprevXY = list()
    PorigXY = list()
    PinHandX = np.zeros((5,1))
    PinHandX[:] = np.nan
    HX = np.zeros((5,1))
    HX[:] = np.nan
    failtest = False
    successtest = False
    eName = ''
    eFrame = 0
    lastRet = 0
    
    for vndx, video in enumerate(videos):
        PprevXY.append(np.zeros((2,1)))
        PorigXY.append(np.zeros((2,1)))
        Dataframe = pd.read_hdf(h5list[vndx])
        numberFrmList.append(len(Dataframe[scorer][bodyparts[0]]['x'].values))
        df_likelihood.append(np.empty((len(bodyparts),numberFrmList[vndx])))
        df_x.append(np.empty((len(bodyparts),numberFrmList[vndx])))
        df_y.append(np.empty((len(bodyparts),numberFrmList[vndx])))
        for bpindex, bp in enumerate(bodyparts):
            df_likelihood[vndx][bpindex,:]=Dataframe[scorer][bp]['likelihood'].values
            df_x[vndx][bpindex,:]=Dataframe[scorer][bp]['x'].values
            df_y[vndx][bpindex,:]=Dataframe[scorer][bp]['y'].values
        
        
        bpindexP = bodyparts.index('Free')
        pellet_test = np.where(df_likelihood[vndx][bpindexP,:] > 0.99)
        cpt = cropPts[vndx]
        zeroPx = np.median(df_x[vndx][bpindexP,pellet_test])+cpt[0]
        zeroPy = np.median(df_y[vndx][bpindexP,pellet_test])+cpt[2]
        PorigXY[vndx][0] = zeroPx
        PorigXY[vndx][1] = zeroPy
        PprevXY[vndx][0] = zeroPx
        PprevXY[vndx][1] = zeroPy
        
    numberFrames = np.min(numberFrmList)
    f = open(ffile,'w')
    for currFrame in range(numberFrames):
        if not findP:
            if pellet_state == 0:
                eName = ' '
                newFrame = currFrame
                es = 'placed'
                while not eName == es:
                    if eventTab == len(eventList):
                        break
                    event = eventList[eventTab]
                    eventTab+=1
                    eName,eFrame = event.split(' - ')
                    eFrame = int(eFrame)
                    newFrame = eFrame
                    if (newFrame >= numberFrames):
                        break
                    elif newFrame < 0:
                        newFrame = 0
                    
                pellet_state  = 1
                rch_sel = 0
                if eventTab == len(eventList):
                    eName = ''
                    eFrame = 0
                else:
                    event = eventList[eventTab]
                    eName,eFrame = event.split(' - ')
                    eFrame = int(eFrame)
                    eventTab+=1
                continue
            elif currFrame == eFrame:
                if eName == 'fail':
                    f.write('%s\t%d\n' % (eName,currFrame))
                elif eName == 'stim':
                    f.write('success\t%d\n' % currFrame)
                elif eName != 'placed':
                    pellet_state  = 0
                continue
    
        retrieving = False
        frmrate = 150
        hand_seen = False
        pellet_on_post = [0,0,0]
        pellet_fallFast = [0,0,0]
        pellet_fallSlow = [0,0,0]
        pellet_seen = [0,0,0]
        tongue_seen = [0,0,0]
        distP = [np.nan,np.nan,np.nan]
        PcurrXY = list()
        deltaPY = np.nan
        for ndx in range(3):
            cpt = cropPts[ndx]
            PcurrXY.append(np.zeros((2 ,1)))
            HcurrXY = np.zeros((2 ,1))
            if ndx == 0:
                nanHP = np.isnan(HX)
                nanHPtot = np.sum(nanHP)
                numHP = HX[~nanHP]
                if nanHPtot < 2:
                    hdelta = -np.nanmean(np.diff(numHP))
                    if (hdelta > 3) and rch_sel == 0:
                        if (currFrame-lastRet) > 150:
                            rch_sel = 1
                            f.write('reach\t%d\n' % currFrame)
                HX = np.roll(HX,1)
                HX[0] = np.nan
            if ndx == 2:
                nanHP = np.isnan(PinHandX)
                nanHPtot = np.sum(nanHP)
                numHP = PinHandX[~nanHP]
                if nanHPtot < 2:
                    hpdelta = -np.nanmean(np.diff(numHP))
                    if hpdelta < -3 and rch_sel == 1:
                        if (currFrame-lastRet) > 150:
                            lastRet = currFrame
                            rch_sel = 0
                            retrieving = True
                            f.write('retrieve\t%d\n' % currFrame)
                PinHandX = np.roll(PinHandX,1)
                PinHandX[0] = np.nan
                
        
            PcurrXY.append(np.zeros((2 ,1)))
            
            if ndx == 0:
                keylist = ['Hand','Pellet','Other']
            elif ndx == 1:
                keylist = ['Pellet','Other']
            else:
                keylist = ['Pellet','Other']
                
            for key in keylist:
                testlist = list()
                ndxlist = list()
                for ptname in cfg['bodyparts'][key]:
                    bpndx = bodyparts.index(ptname)
                    ndxlist.append(bpndx)
                    bp_test = df_likelihood[ndx][bpndx,currFrame]
                    testlist.append(bp_test)
                    
                if key == 'Other':
                    bpndx = bodyparts.index('Tongue')
                    bp_test = df_likelihood[ndx][bpndx,currFrame]
                    if np.amax(bp_test) > 0.9:
                        tongue_seen[ndx] = 1
                    continue
                elif key == 'Hand':
                    if np.amax(testlist) > 0.9: 
                        bpndx = ndxlist[np.argmax(testlist)]
                        points = [int(df_x[ndx][bpndx,currFrame]),int(df_y[ndx][bpndx,currFrame])]
                        HcurrXY[0] = points[0]+cpt[0]
                        HcurrXY[1] = points[1]+cpt[2]
                        HX[0] = HcurrXY[0]
                        hand_seen = True
                elif key == 'Pellet':
                    if np.amax(testlist) > 0.9:
                        bpndx = ndxlist[np.argmax(testlist)]
                        if bodyparts[bpndx] == 'InHand' and ndx < 2:
                            PprevXY[ndx][0] = np.nan
                            PprevXY[ndx][1] = np.nan
                            continue
                        points = [int(df_x[ndx][bpndx,currFrame]),int(df_y[ndx][bpndx,currFrame])]
                        
                        if bodyparts[bpndx] == 'InHand' and ndx == 2:
                            PinHandX[0] = points[0]+cpt[0]
                            hand_seen = True
                        pellet_seen[ndx] = 1
                        PcurrXY[ndx][0] = points[0]+cpt[0]
                        PcurrXY[ndx][1] = points[1]+cpt[2]
                        distP[ndx] = np.linalg.norm(PorigXY[ndx]-PcurrXY[ndx])
                        if distP[ndx] < 5:
                            pellet_on_post[ndx] = 1
                        if not np.isnan(PprevXY[ndx][0]):
                            deltaPY = PprevXY[ndx][1]-PcurrXY[ndx][1]
                            deltaPX = PprevXY[ndx][0]-PcurrXY[ndx][0]
                            if ndx == 2:
                                deltaPY = -deltaPY
                            fallA = deltaPY < -5 or deltaPX < -5
                            fallB = deltaPY < -15 or deltaPX < -15
                            fallC = abs(deltaPY) < 50 and abs(deltaPX) < 50
                            if fallA and fallC:
                                pellet_fallSlow[ndx] = 1
                            if fallB and fallC:
                                pellet_fallFast[ndx] = 1
                        PprevXY[ndx] = PcurrXY[ndx]
                    else:
                        PprevXY[ndx][0] = np.nan
                        PprevXY[ndx][1] = np.nan
        
        if sum(tongue_seen) > 0:
            PinHandX[1] = np.nan
        if sum(pellet_seen) > 1 and not act_sel == 3:
            pellet_seen = True
        elif sum(pellet_seen) == 3 and act_sel == 3:
            pellet_seen = True
        else:
            pellet_seen = False
        if sum(pellet_on_post) > 1:
            pellet_on_post = True
        else:
            pellet_on_post = False
        
        failbool = False
        
        if act_sel == 3:
            if pellet_on_post:
                act_sel = 4
                sys_timer = [0,0,0,0,0]
#                print('pellet placed')
#                print(currFrame)
                failtest = False
                successtest = False
                rch_sel = 0
                
        if act_sel == 4:
            if failtest:
                if pellet_on_post:
                    sys_timer[2] = 0
                    sys_timer[1]+=1
                    if sys_timer[1] > 10:
                        failtest = False
#                        print('false alarm')
                else:
                    sys_timer[2]+=1
                    sys_timer[1] = 0
                    
                if sys_timer[2] > 10:
                    failbool = True
#                    print('reach fail')
                    
            if (sum(pellet_fallSlow) > 0 or sum(pellet_fallFast) > 0) and not pellet_on_post:
                sys_timer[4]+=1
                fallTestA = sum(pellet_fallSlow) > 0 and sys_timer[4] == 4
                fallTestB = sum(pellet_fallFast) > 0 and sys_timer[4] == 3
                fallTestC = sum(pellet_fallSlow) > 1
            
                if fallTestA or fallTestB or fallTestC:
                    if not failtest:
                        successtest = False
                        failtest = True
                        sys_timer[1] = 0
                        sys_timer[2] = 0
#                        print('fail test')
            else:
                sys_timer[4] = 0
            
            if successtest:
                if not hand_seen and not pellet_on_post:
                    sys_timer[3]+=1
                    if sys_timer[3] > 10:
                        act_sel = 3
#                        print('reach success!')
                        f.write('success\t%d\n' % currFrame)
                elif pellet_on_post:
                    successtest = False
                else:
                    sys_timer[3] = 0
                    
            if retrieving and not pellet_on_post:
                if not successtest:
                    successtest = True
                    sys_timer[3] = 0
#                    print('success test')

            if not pellet_seen:
                sys_timer[0]+=1
                if sys_timer[0] > frmrate*5:
#                    print('pellet lost')
                    act_sel = 3
            else:
                sys_timer[0] = 0
            
            if failbool:
                act_sel = 3
                f.write('fail\t%d\n' % currFrame)
    
    f.close()
            
if __name__ == '__main__':
    
    prep()